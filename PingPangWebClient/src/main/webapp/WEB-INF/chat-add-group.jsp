<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <title></title> 
  <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/font.css">
  <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/login.css">
  <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/xadmin.css">
  
  <script src="${httpServletRequest.getContextPath()}/jquery.min.js"></script>
  <script src="${httpServletRequest.getContextPath()}/layer.js"></script>
  <script src="${httpServletRequest.getContextPath()}/layui.js"></script>
  <script type="text/javascript" src="${httpServletRequest.getContextPath()}/x-admin/js/xadmin.js"></script> 
</head>
<body>

<div class="layui-fluid">
     <div class="layui-row">
       <form class="layui-form" id="addGroup" action="" method="POST">
   
       <input name="groupName" placeholder="群昵称"  type="text" autocomplete="off"  lay-verify="required|groupName" class="layui-input" >
            <hr class="hr15">
       <input name="groupCode" placeholder="群编码"  type="text" autocomplete="off" lay-verify="required|groupCode" class="layui-input">
            <hr class="hr15">
       <input name="groupPurpose" placeholder="备注"  type="text" autocomplete="off" lay-verify="required|groupPurpose" class="layui-input">
            <hr class="hr15">
       <hr class="hr15">
       <button class="layui-btn" lay-filter="addGroup"  lay-submit="" style="width:100%;">添加群组</button>
      </form>
     </div>
</div>    
  <script>
             layui.use(['form','layer','laydate' ], function() {
					var form = layui.form
					   ,layer = layui.layer
					   ,layedit = layui.layedit
					   ,laydate = layui.laydate;
					 
					//自定义验证规则
					form.verify({
						groupName : function(value) {
							if (value.length < 2 || value.length >8) {
								return '代号至少需要2-8位字符';
							}
						},
						groupCode : [ /^[0-9A-Za-z]{2,6}$/, '字母和数字2-8位' ],
						groupPurpose : function(value) {
							if (value.length >50) {
								return '备注最多50位字符';
							}
						}
					});

					//监听提交
					form.on('submit(addGroup)',function(data) {
						$.ajax({
					        url:"${httpServletRequest.getContextPath()}/group/db-group-add",
					        data:getFormJson("#addGroup"),
					        type:"Post",
					        dataType:"json",
					        async: false,
					        success:function(data){
					        	var oldUser=data;
					        	//console.log(oldUser);
					            if("S"==oldUser.CODE){
					            	layer.msg('执行成功', {icon: 1});
					            	//关闭当前frame
			                        //xadmin.close();
			                        // 可以对父窗口进行刷新 
			                        //xadmin.father_reload();
					            	//$.messager.alert('成功',oldUser.MESSAGE);	
					            }else{
					            	layer.msg(oldUser.MESSAGE, {icon: 1});
					            	//$.messager.alert('错误',oldUser.MESSAGE);	
					            }
					        },
					        error:function(data){
					        	layer.msg('错误',data.msg);
					        }
					    });
						return false;
	                });
				});
             
             //将form中的值转换为键值对。
             function getFormJson(frm) {
                 var o = {dosubmit:1};
                 var a = $(frm).serializeArray();
                 $.each(a, function () {
                     if (o[this.name] !== undefined) {
                         if (!o[this.name].push) {
                             o[this.name] = [o[this.name]];
                         }
                         o[this.name].push(this.value || '');
                     } else {
                         o[this.name] = this.value || '';
                     }
                 });
                 return o;
             }     
		</script>
</body>
</html>