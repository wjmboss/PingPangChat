<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="x-admin-sm">
     <head>
        <meta charset="UTF-8">
        <title>PingPang管理页面</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/font.css">
        <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/xadmin.css">
        <script src="${httpServletRequest.getContextPath()}/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${httpServletRequest.getContextPath()}/x-admin/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5" id="sreach">
                            
                                 <div class="layui-input-inline layui-show-xs-block">
                                 <!-- 1.绑定上线 2.下线 3.单聊 4.群聊 5.获取用户信息 6获取群组用户信息 -->
                                    <select name="cmd">
                                        <option value="">所有类型</option>
                                        <option value="3">单聊</option>
                                        <option value="4">群聊</option>
                                    </select>
                                </div>
                                
                                <div class="layui-input-inline layui-show-xs-block">
                                 <!-- 信息状态0未发送 1已发送 -1删除 -->
                                    <select name="status">
                                        <option value="">所有状态</option>
                                        <option value="0">未发送</option>
                                        <option value="1">已发送</option>
                                        <option value="-1">删除</option>
                                    </select>
                                </div>
                                
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="fromUserCode"  placeholder="发送用户代码" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="acceptUserCode"  placeholder="接收用户代码" autocomplete="off" class="layui-input">
                                </div>
                                
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input"  autocomplete="off" placeholder="开始日" name="startDate" id="start">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input"  autocomplete="off" placeholder="截止日" name="endDaet" id="end">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <input type='button' class="layui-btn"  onclick="sreach();" value="查询">
                                </div>
                            </form>
                        </div>
                        <div class="layui-card-header">
                            <button class="layui-btn layui-btn-xs" onclick="managerAll(0)"><i class="layui-icon"></i>批量未发</button>
                            <button class="layui-btn layui-btn-warm" onclick="managerAll(1)"><i class="layui-icon"></i>批量已发</button>
                            <button class="layui-btn layui-btn-danger" onclick="managerAll(-1)"><i class="layui-icon"></i>批量删除</button>
                        </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form" id="msgList">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </body>
    <script>
    layui.use(['laydate','form','table'],
            function() {
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '#start' //指定元素
                });

                //执行一个laydate实例
                laydate.render({
                    elem: '#end' //指定元素
                });
                
                var table = layui.table;
                //第一个实例
                table.render({
                  elem: '#msgList',
                  height: 312,
                  url: '${httpServletRequest.getContextPath()}/msg/db-list', //数据接口
                  method:'post',
                  contentType: 'application/json; charset=UTF-8',
                  initSort: {
                	    field: 'createDate' //排序字段，对应 cols 设定的各字段名
                	    ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
                	  },
                  //where:JSON.stringify(layui.form.field),
                  page: true, //开启分页
                  cols: [
                	      [
                	    	{type: 'checkbox',field: 'id',fixed:'left'},
    		                {field: 'cmd', title: '信息类型', width:90,templet:'<div>{{ cmdFormat(d.cmd)}}</div>'},
    		                {field: 'group.groupCode', title: '群编码', width:90,templet: function(d){
    		                    return null==d.group?"":d.group.groupCode;
    		                }},
    		                {field: 'group.groupName', title: '群名称', width:90,templet: function(d){
    		                    return null==d.group?"":d.group.groupName;
    		                }},
    		                {field: 'from.userCode', title: '发送用户代码', width:90,templet: function(d){
    		                    return d.from.userCode;
    		                }},
    		                {field: 'from.userName', title: '发送用户名称', width:90,templet: function(d){
    		                    return d.from.userName;
    		                }},
    		                {field: 'from.ip', title: '发送IP', width:90,templet: function(d){
    		                    return d.from.ip;
    		                }},
    		                {field: 'accept.userCode', title: '接收用户代码', width:90,templet: function(d){
    		                    return d.accept.userCode;
    		                }},,
    		                {field: 'accept.userName', title: '接收用户名称', width:90,templet: function(d){
    		                    return d.accept.userName;
    		                }},
    		                {field: 'accept.ip', title: '接收IP', width:90,templet: function(d){
    		                    return d.accept.ip;
    		                }},
    		                {field: 'msg', title: '用户信息', width:90},
    		                {field: 'createDate', title: '创建日期', width:120},
    		                {field: 'status', title: '状态', width: 90,templet:'<div>{{ statusFormat(d.status)}}</div>'},
    		                {field: 'right', title: '操作', width: 370, toolbar:"#barDemo"}
                          ]
                	    ]
                });
                
            });
    
    //1.绑定上线 2.下线 3.单聊 4.群聊 5.获取用户信息 6获取群组用户信息
    function cmdFormat(statu) {
    	var msg="";
        if(1==statu){
        	msg="绑定上线";
        }else if(2==statu){
        	msg="下线";
        }else if(3==statu){
        	msg="单聊";
        }else if(4==statu){
        	msg="群聊";
        }else if(5==statu){
        	msg="获取用户信息";
        }else if(6==statu){
        	msg="获取群组用户信息";
        }else{
        	msg="未识别 ";
        }
        return msg;
    }  
    
    //状态翻译 信息状态0未发送 1已发送 -1删除
    function statusFormat(statu) {
    	var msg="";
        if(-1==statu){
        	msg="删除";
        }else if(0==statu){
        	msg="未发送";
        }else if(1==statu){
        	msg="已发送";
        }else{
        	msg=" 未识别 ";
        }
        return msg;
    }  
    
    /**
     *type 状态0未发送 1已发送 -1删除
     */
    function managerMsg(id,type){
    	 var msg="";
    	 var url="";
    	 if(0==type){
    		 msg="确认未发送吗？";
    		 url="${httpServletRequest.getContextPath()}/msg/db-downMsg";
    	 }else if(1==type){
    		 msg="确认已发送吗？";
    		 url="${httpServletRequest.getContextPath()}/msg/db-upMsg";
    	 }else if(-1==type){
    		 msg="确认要删除吗？";
    		 url="${httpServletRequest.getContextPath()}/msg/db-delMsg";
    	 }else{
    		 layer.msg('操作未识别!', {icon: 1});
   		     return false;
    	 }
    	 
    	 layer.confirm(msg,function(index){
       	  $.ajax({
       		  type: 'POST',
       		  async: false,
       		  dataType:'json',
       		  url:url,
       		  data:"id="+id,
       		  success: function(data){
       			  if("S"==data.CODE){
       				  layer.msg('执行成功', {icon: 1});
       				 // window.parent.location.reload();
       				  layui.table.reload("msgList", { //此处是上文提到的 初始化标识id
       		                where: {
       		                    //key: { //该写法上文已经提到
       		                        //type: item.type, id: item.id
       		                    //}
       		                }
       		            });
       			  }else{
       				if(null==data.MESSAGE){
       				  layer.msg('执行失败', {icon: 1});
       				}else{
       				  layer.msg(data.MESSAGE, {icon: 1});
       				}
       			  }
       		  },
       		  error : function(errorMsg) {
                     layer.msg(errorMsg,{time:2000,end:function(){
       			  }});
       		  }
       		});
       });
    }
    
    /**
     *type 状态0未发送 1已发送 -1删除
     */
    function managerAll (type) {
    	if(0==type){
   		 msg="确认要未发送吗？";
   		 url="${httpServletRequest.getContextPath()}/msg/db-downMsgAll";
   	    }else if(1==type){
   		 msg="确认要已发送吗？";
   		 url="${httpServletRequest.getContextPath()}/msg/db-upMsgAll";
   	    }else if(-1==type){
   		 msg="确认要删除吗？";
   		 url="${httpServletRequest.getContextPath()}/msg/db-delMsgAll";
   	    }else{
   	    	return;
   	    }
        var checkStatus = layui.table.checkStatus('msgList').data;
         var ids = [];
         for(var i=0;i<checkStatus.length;i++){
       	  ids.push(checkStatus[i].id);
         }
         layer.confirm(msg,function(index){
             //捉到所有被选中的，发异步进行删除
              $.ajax({
               		  type: 'POST',
               		  async: false,
               		  dataType:'json',
               		  url:url,
               		  data:"ids="+ids.toString(),
               		  success: function(data){
               			  if("S"==data.CODE){
               				  layer.msg('执行成功', {icon: 1});
               				 // window.parent.location.reload();
               				  layui.table.reload("msgList", { //此处是上文提到的 初始化标识id
               		                where: {
               		                    //key: { //该写法上文已经提到
               		                        //type: item.type, id: item.id
               		                    //}
               		                }
               		            });
               			  }
               			  //layer.msg(data.MESSAGE,{time:2000,end:function(){
               			  //}});
               		  },
               		  error : function(errorMsg) {
                             layer.msg(errorMsg,{time:2000,end:function(){
               			  }});
               		  }
               		});
         }); 
       }
    
    function sreach(){
    	var param=JSON.stringify(getFormJson($('#sreach')));
    	//alert(param);
    	layui.table.reload("msgList", { //此处是上文提到的 初始化标识id
            where: {
            	search:param
            }
        });
    	return false;
    }
    

    //将form中的值转换为键值对。
    function getFormJson(frm) {
        var o = {dosubmit:1};
        var a = $(frm).serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }
    
    </script>
    <script type="text/html" id="barDemo">
        <button type="button" class="layui-btn layui-btn-xs" onclick="managerMsg('{{d.id}}','0')">未发送</button>
		<button type="button" class="layui-btn layui-btn-warm" onclick="managerMsg('{{d.id}}','1')">已发送</button>
        <button type="button" class="layui-btn layui-btn-danger" onclick="managerMsg('{{d.id}}','-1')">删除</button>
</script>
</html>