package com.pingpang.load;

import com.pingpang.config.SpringContextUtil;
import com.pingpang.redis.service.RedisService;

public abstract class LoadInterface {

	// redis操作
    public final RedisService redisService = SpringContextUtil.getBean(RedisService.class);
	
    /**
	   * 获取一个地址
	 * @return
	 */
	public abstract String getAddress();
}
