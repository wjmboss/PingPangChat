 **使用方法**

 **项目启动需要安装的环境** 

   安装 redis

   安装 zookeeper


 **环境配置** 
 
   PingPangServer修改配置文件信息

   ```
   #后台绑定端口

   netty.ip=127.0.0.1

   netty.port=8086

   #-----------------redis--------------------------

   # REDIS (RedisProperties)

   # Redis数据库索引（默认为0）

   spring.redis.database=0

   # Redis服务器地址

   spring.redis.host=127.0.0.1

   # Redis服务器连接端口

   spring.redis.port=6379

   # Redis服务器连接密码（默认为空）

   spring.redis.password=123654


   #zk地址配置

   zk.address=127.0.0.1:2181
  ``` 

   PingPangWebClient修改配置文件信息

   ```
   #-----------------redis--------------------------

   # REDIS (RedisProperties)

   # Redis数据库索引（默认为0）

   spring.redis.database=0

   # Redis服务器地址

   spring.redis.host=127.0.0.1

   # Redis服务器连接端口

   spring.redis.port=6379

   # Redis服务器连接密码（默认为空）

   spring.redis.password=123654


   #zk地址配置

   zk.address=127.0.0.1:2181
  ``` 
 
 **2.[初始化脚本](https://gitee.com/0X00000000/PingPangChat/blob/2.3.0/PingPangDB/DB/2020042701.sql)** 

 **3.启动服务端** 


```
java -jar PingPangServer.war
```


 **4.启动客户端** 


```
java -jar PingPangWebClient.war
```


 **5.访问客户端程序** 

```

https://127.0.0.1/

```

 **6.视频聊天目前借助的是peerjs** 

这里不是必须的

需要自行搭建peerjs的node的环境程序

```

var fs = require('fs');
 var PeerServer = require('peer').PeerServer;

/*
var options = {
  key: fs.readFileSync('key/server.key'),
  cert: fs.readFileSync('key/server.crt')
}
*/

var options = {
    key: fs.readFileSync('peerjs+2-key.pem'),
    cert: fs.readFileSync('peerjs+2.pem')
}

var server = PeerServer({
  port: 9000,
  ssl: options,
  path:"/"
});

/** 构建html页 */
var https = require('https');
var serveIndex = require('serve-index');
var express = require("express");       
var htmlApp = express();
htmlApp.use(serveIndex('./html'));
htmlApp.use(express.static("./html"))
var httpsServer = https.createServer(options, htmlApp);
httpsServer.listen(9001, "0.0.0.0");


```


```
node server.js
```

