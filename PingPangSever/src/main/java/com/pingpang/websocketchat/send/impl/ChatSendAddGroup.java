package com.pingpang.websocketchat.send.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pingpang.util.StringUtil;
import com.pingpang.websocketchat.ChannelManager;
import com.pingpang.websocketchat.ChatGroup;
import com.pingpang.websocketchat.Message;
import com.pingpang.websocketchat.send.ChatSend;

import io.netty.channel.ChannelHandlerContext;

public class ChatSendAddGroup extends ChatSend{

	@Override
	public void send(Message message, ChannelHandlerContext ctx) throws JsonProcessingException {
		if(StringUtil.isNUll(message.getGroup().getGroupCode())) {
			return;
		}
		
		message.setFrom(userService.getUser(message.getFrom()));

		Map<String,String> queryMap=new HashMap<String,String>();
		queryMap.put("groupCode",message.getGroup().getGroupCode());
	    ChatGroup group=this.userGroupService.getGroup(queryMap);
	    message.setGroup(group);
	    
	    if (!ChannelManager.isExit(message.getGroup(), message.getFrom().getUserCode())) {
			ChannelManager.addGroup(message.getGroup(), message.getFrom());
		}
	}
}
